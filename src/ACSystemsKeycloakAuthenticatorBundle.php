<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle;

use ACSystems\KeycloakAuthenticatorBundle\DependencyInjection\ACSystemsKeycloakAuthenticatorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ACSystemsKeycloakAuthenticatorBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return $this->extension ?? new ACSystemsKeycloakAuthenticatorExtension();
    }
}
