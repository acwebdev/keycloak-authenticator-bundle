<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// @formatter:off
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(ACSystemsKeycloakAuthenticatorExtension::EXTENSION_ALIAS);
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->arrayNode('keycloak_authenticator')
                    ->append($this->getClient())
                    ->append($this->getKeycloakUri())
                    ->append($this->getKeycloakRealm())
                ->end();

        return $treeBuilder;
    }

    public function getClient(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('client_id');
        $node
            ->defaultValue('@azp')
            ->validate()
                ->ifTrue(static function ($v) {
                    return !is_string($v);
                })
                ->thenInvalid('client_id must be a string.')
            ->end();
        return $node;
    }

    public function getKeycloakUri(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('base_uri');
        $node
            ->validate()
                ->ifTrue(static function ($v) {
                    return !is_string($v);
                })
                ->thenInvalid('base_uri must be a string');

        return $node;
    }

    public function getKeycloakRealm(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('realm');
        $node->defaultNull();

        return $node;
    }
}
// @formatter:on
