<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ACSystemsKeycloakAuthenticatorExtension extends Extension
{
    public const EXTENSION_ALIAS = 'acsystems_keycloak_authenticator';

    public function getAlias(): string
    {
        return self::EXTENSION_ALIAS;
    }

    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $config = array_merge(['keycloak_authenticator' => [
            'client_id' => '@azp',
            'jwks_uri' => ''
        ]], $config);

        foreach ($config as $namespace => $values) {
            foreach ($values as $key => $value) {
                $container->setParameter("$namespace.$key", $value);
            }
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
    }
}
