<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Provider;

interface JwkProviderInterface
{
    public function getJwk(string $token, string $kid): mixed;
}
