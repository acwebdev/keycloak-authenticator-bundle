<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Provider;

use ACSystems\KeycloakAuthenticatorBundle\DependencyInjection\ACSystemsKeycloakAuthenticatorExtension;
use Firebase\JWT\JWK;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class JwksUriJwkProvider implements JwkProviderInterface
{
    private const JWKS_CACHE = ACSystemsKeycloakAuthenticatorExtension::EXTENSION_ALIAS . '_jwks_';

    public function __construct(
        private readonly CacheInterface $cache,
        private readonly ?string $baseUrl,
        private readonly ?string $realm
    ) {
    }

    /**
     * @throws InvalidArgumentException|JsonException
     */
    public function getJwk(string $token, string $kid): array
    {
        $realm = $this->getRealm($token);

        $jwks = $this->getJson($realm);
        if (isset($jwks[$kid])) {
            return JWK::parseKeySet($jwks);
        }

        $this->cache->delete($this->getCacheKey($realm));
        return JWK::parseKeySet($this->getJson($realm));
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getJson(string $realm): array
    {
        $url = "{$this->baseUrl}realms/$realm/protocol/openid-connect/certs";

        return $this->cache->get($this->getCacheKey($realm), static function (ItemInterface $item) use ($url): array {
            $item->expiresAfter(3600);
            return json_decode(file_get_contents($url), true, 512, JSON_THROW_ON_ERROR) ?: [];
        });
    }

    private function getCacheKey(string $realm): string
    {
        return self::JWKS_CACHE . $realm;
    }

    /**
     * @throws JsonException
     */
    private function getRealm(string $token): string
    {
        if ($this->realm !== null) {
            return $this->realm;
        }

        $tokenParts = explode('.', $token);
        $tokenBody = json_decode(base64_decode($tokenParts[1]), true, 512, JSON_THROW_ON_ERROR);

        $issuerParts = explode('/', $tokenBody['iss']);
        return end($issuerParts);
    }
}
