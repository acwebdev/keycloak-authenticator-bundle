<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class KeycloakUser implements ParsedTokenInterface, UserInterface
{
    public function __construct(
        private readonly string $id,
        private readonly string $username,
        private readonly array $roles,
        private readonly array $parsedToken,
        private readonly string $token
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getAttribute(string $attribute): mixed
    {
        return $this->parsedToken[$attribute] ?? null;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getPassword(): ?string
    {
        return null;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
    }

    public function getRealm(): string
    {
        $issuerParts = explode('/', $this->parsedToken['iss']);
        return end($issuerParts);
    }
}
