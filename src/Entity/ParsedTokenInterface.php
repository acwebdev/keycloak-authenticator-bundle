<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Entity;

interface ParsedTokenInterface
{
    public function getId(): string;
    public function getUsername(): string;
    public function getRoles(): array;
    public function getAttribute(string $attribute): mixed;
}
