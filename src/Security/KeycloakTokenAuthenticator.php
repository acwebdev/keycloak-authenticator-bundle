<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Security;

use ACSystems\KeycloakAuthenticatorBundle\Service\KeycloakParsedTokenFactory;
use ACSystems\KeycloakAuthenticatorBundle\Service\TokenDecoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class KeycloakTokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly KeycloakParsedTokenFactory $parsedTokenFactory,
        private readonly TokenDecoderInterface $tokenDecoder
    ) {
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): bool
    {
        $authHeader = $request->headers->get('Authorization');
        return !empty($authHeader);
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get('Authorization');
        if (empty($token)) {
            throw new CustomUserMessageAuthenticationException('Bearer token not found in headers');
        }

        $decodedToken = $this->tokenDecoder->decodeToken($token);
        $user = $this->parsedTokenFactory->createFromToken($decodedToken, $token);
        return new SelfValidatingPassport(new UserBadge(
            $user->getUserIdentifier(),
            fn () => $user,
        ));
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return new JsonResponse(['error' => $exception->getMessage(), Response::HTTP_UNAUTHORIZED]);
    }
}
