<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Exception;

use RuntimeException;

class JWTDecoderException extends RuntimeException
{
}
