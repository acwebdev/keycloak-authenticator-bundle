<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Service;

use ACSystems\KeycloakAuthenticatorBundle\Entity\KeycloakUser;

class KeycloakParsedTokenFactory
{
    public function __construct(
        private readonly string $clientId = '@azp'
    ) {
    }

    public function createFromToken(array $parsedToken, string $token): KeycloakUser
    {
        $clientId = $this->clientId === '@azp'
            ? $parsedToken['azp']
            : $this->clientId;

        $roles = $this->getRoles($parsedToken, $clientId);

        return new KeycloakUser(
            $parsedToken['sub'],
            $parsedToken['preferred_username'],
            $roles,
            $parsedToken,
            $token
        );
    }

    protected function getRoles(array $token, string $clientId): array
    {
        if (empty($token['resource_access'][$clientId])) {
            return ['ROLE_USER'];
        }

        $kcRoles = $token['resource_access'][$clientId]['roles'] ?? [];
        $roles = array_map(static fn (string $role) => !str_contains($role, 'ROLE_') ? 'ROLE_' . $role : $role, $kcRoles);
        // Required by Symfony
        $roles[] = 'ROLE_USER';

        return $roles;
    }
}
