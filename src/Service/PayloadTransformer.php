<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Service;

class PayloadTransformer
{
    public function transformPayload(object $payload): array
    {
        $arrayPayload = get_object_vars($payload);
        foreach ($arrayPayload as $key => $value) {
            if (is_object($value)) {
                $arrayPayload[$key] = $this->transformPayload($value);
            }
        }
        return $arrayPayload;
    }
}
