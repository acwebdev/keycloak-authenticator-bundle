<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Service;

interface TokenDecoderInterface
{
    public function decodeToken(string $token): array;
}
