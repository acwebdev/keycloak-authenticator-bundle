<?php

declare(strict_types=1);

namespace ACSystems\KeycloakAuthenticatorBundle\Service;

use ACSystems\KeycloakAuthenticatorBundle\Exception\JWTDecoderException;
use ACSystems\KeycloakAuthenticatorBundle\Provider\JwkProviderInterface;
use Exception;
use Firebase\JWT\JWT;
use JsonException;

class BearerTokenDecoder implements TokenDecoderInterface
{
    public function __construct(
        private readonly JwkProviderInterface $jwkProvider
    ) {
    }

    public function decodeToken(string $token): array
    {
        $jwt = preg_replace('/^Bearer\s/', '', $token);
        $encodedJwtHead = substr($jwt, 0, strpos($jwt, '.'));
        try {
            $jwtHeader = json_decode(base64_decode($encodedJwtHead), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $ex) {
            throw new JWTDecoderException('Unable to decode JWT header: ' . $ex->getMessage());
        }

        if (empty($jwtHeader['kid']) || empty($jwtHeader['alg'])) {
            throw new JWTDecoderException('Invalid kid or algorithm');
        }

        $keyId = $jwtHeader['kid'];

        try {
            $jwk = $this->jwkProvider->getJwk($token, $keyId);
            if ($jwk === null) {
                throw new JWTDecoderException('No key found for decoding the token');
            }

            $decoded = JWT::decode($jwt, $jwk);
        } catch (Exception $ex) {
            throw new JWTDecoderException($ex->getMessage(), 0, $ex);
        }

        return (new PayloadTransformer())->transformPayload($decoded);
    }
}
