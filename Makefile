SHELL := /bin/bash
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: dev shell rshell quality tests run-tests
.SILENT:

shell:
	docker exec -it -u nginx web ash

rshell:
	docker exec -it -u root web ash

dev: killenv updateenv startenv

killenv:
	docker stop $$(docker ps -a -q) > /dev/null 2>&1 || true
	docker rm $$(docker ps -a -q) > /dev/null 2>&1 || true

updateenv:
	docker-compose pull

startenv:
	docker-compose up -d
	make post-start

post-start:
	docker-compose exec web chown -R nginx:nginx /var/www/html/var || true
	docker-compose exec web chmod -R 777 /var/www/html/var

setup-pipeline:
	@echo '### Installing dependencies'
	curl -o composer.phar https://getcomposer.org/composer-stable.phar
	php -d memory_limit=-1 composer.phar install -v --no-scripts --no-ansi --no-interaction --no-progress

quality:
	@echo "Installing packages to make sure we can run all quality checks"
	composer install --no-scripts --ignore-platform-reqs
	@echo "Updating jakzal/phpqa:1.77-php8.1-alpine docker image"
	docker pull jakzal/phpqa:1.77-php8.1-alpine
	@echo '### Running php-cs-fixer'
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine php-cs-fixer fix src
	@echo '### Running phpcs'
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine phpcs -s --standard=phpcs.xml
	@echo '### Running phpmd'
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine phpmd src ansi phpmd
	@echo '### Running phpcpd'
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine phpcpd --fuzzy src
	@echo '### Running phpstan'
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine phpstan -vvv --debug analyze src
	@echo "### Running parallel-lint"
	docker run --init -t --rm -v "$(ROOT_DIR):/project" -w /project jakzal/phpqa:1.77-php8.1-alpine parallel-lint src

tests:
	docker exec -u nginx web make run-tests

run-tests:
	@echo '### Running tests'
	php vendor/bin/codecept run --steps
