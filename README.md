# Keycloak Symfony Authenticator Bundle

The goal of this bundle is to provide a Keycloak token authenticator for Symfony.

![License](https://img.shields.io/badge/license-MIT-brightgreen)
[![PHP](https://img.shields.io/badge/%3C%2F%3E-PHP%208.1-blue)](https://www.php.net/) 
[![Code Style](https://img.shields.io/badge/code%20style-psr--12-darkgreen)](https://www.php-fig.org/psr/psr-2/)

## Documentation

### Quick start

#### Installation

Install the package from packagist using composer

```console
composer require acsystems/keycloak-authenticator-bundle
```

Add the bundle.

`config/bundles.php`
```php
return [
    ACSystems\KeycloakAuthenticatorBundle\ACSystemsKeycloakAuthenticatorBundle::class => ['all' => true]
];
```

Set up Symfony Security to use the custom authenticator.

`config/packages/security.yaml`
```yaml
security:
  firewalls:
    main:
      stateless: true
      custom_authenticators:
        - ACSystems\KeycloakAuthenticatorBundle\Security\KeycloakTokenAuthenticator

  access_control:
    # ...
```

Add your keycloak base url and realm

`config/packages/keycloak_authenticator.yaml:`
```yaml
acsystems_keycloak_authenticator:
  keycloak_authenticator:
    base_uri: 'https://example.com/'
    realm: 'example-realm'
```

#### Configurable parameters

| Name | Type | Usage |
| --- | --- | --- |
| base_uri | string | URL to your keycloak instance |
| realm | optional string | Realm name, will be derived if not present |
| client_id | optional string | Human readable client_id, will be derived if not present |

### Upgrading
For version migrations instructions see [upgrade instructions](./UPGRADE.md).

### Supported platforms

These are the platforms which are officially supported by this package.
Any other versions might work but is not guaranteed.

| Platform | Version |
|----------|--------:|
| PHP      |    ^8.1 |
| Symfony  |    ^6.1 |

### Contributing

Please read our [contribution guidelines](./CONTRIBUTING.md) before contributing.
