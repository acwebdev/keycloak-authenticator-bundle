<?php

namespace Tests\Unit;

use ACSystems\KeycloakAuthenticatorBundle\Entity\KeycloakUser;
use Tests\Support\UnitTester;

class TokenAuthenticatorCest
{
    public function authenticateTest(UnitTester $I): void
    {
        $jwt = $I->grabJwt();
        $request = $I->haveRequest(['Authorization' => 'Bearer ' . $jwt]);
        $tokenAuthenticator = $I->grabTokenAuthenticator();

        $passport = $tokenAuthenticator->authenticate($request);
        $user = $passport->getUser();

        $I->assertInstanceOf(KeycloakUser::class, $user);
        $I->assertEquals('test-id', $user->getUserIdentifier());
        $I->assertEquals('Tester Person', $user->getAttribute('name'));
    }
}
