<?php

declare(strict_types=1);

namespace Tests\Support;

use ACSystems\KeycloakAuthenticatorBundle\Provider\JwkProviderInterface;
use ACSystems\KeycloakAuthenticatorBundle\Security\KeycloakTokenAuthenticator;
use ACSystems\KeycloakAuthenticatorBundle\Service\BearerTokenDecoder;
use ACSystems\KeycloakAuthenticatorBundle\Service\KeycloakParsedTokenFactory;
use Firebase\JWT\JWK;
use Symfony\Component\HttpFoundation\Request;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause($vars = [])
 *
 * @SuppressWarnings(PHPMD)
 */
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    private ?Request $request = null;

    private function grabTestFile(string $file): string
    {
        $location = codecept_data_dir($file);
        return file_get_contents($location);
    }

    public function grabJwt(): string
    {
        return $this->grabTestFile('jwt');
    }

    public function grabJwk(): array
    {
        $key = $this->grabTestFile('jwk');
        return JWK::parseKeySet(json_decode($key, true, 512, JSON_THROW_ON_ERROR));
    }

    public function haveRequest(array $headers): Request
    {
        $request = Request::create('');
        foreach ($headers as $header => $value) {
            $request->headers->set($header, $value);
        }
        return $request;
    }

    public function grabTokenAuthenticator(): KeycloakTokenAuthenticator
    {
        return new KeycloakTokenAuthenticator(
            new KeycloakParsedTokenFactory(),
            new BearerTokenDecoder(new class($this) implements JwkProviderInterface {
                public function __construct(private readonly UnitTester $tester) { }

                public function getJwk(string $token, string $kid): mixed
                {
                    return $this->tester->grabJwk()[$kid];
                }
            })
        );
    }
}
