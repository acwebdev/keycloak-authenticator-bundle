# Contributing

Thank you for considering a contribution to this project. Please read the steps below to get started.

### Code Style

Please follow the [PSR-12 Coding Style Guide](https://www.php-fig.org/psr/psr-12/) when contributing
to this project. Pull requests with violations will be denied.
